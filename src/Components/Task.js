import React from "react";

export default function Task({ task, colours }) {
  const nestedTasks = (task.children || []).map((task) => {
    return <Task key={task.id} task={task} colours={colours} type="child" />;
  });

  return (
    <div
      key={task.id}
      style={{
        marginLeft: "25px",
        marginTop: "10px",
        // border: "solid",
      }}
    >
      <div
        style={{
          borderBottom: "1px solid grey",
          paddingLeft: "5px",
          paddingBottom: "10px",
          ...(task.complete
            ? { textDecoration: "line-through", color: "grey" }
            : null),
        }}
      >
        <div style={{ fontWeight: 600 }}>{task.content}</div>
        {task.labels.length !== 0 && task.labels[0] !== undefined
          ? task.labels.map((label) => (
              <span
                key={label.id}
                style={{
                  paddingRight: "0.5rem",
                  color: colours[label.color],
                }}
              >
                {label.name}
              </span>
            ))
          : null}
        <div>
          {task.comments.length !== 0 ? (
            <ul
              style={{
                margin: "0",
                paddingLeft: "20px",
                listStyle: "None",
              }}
            >
              {task.comments.map((comment) => (
                <li key={comment.id} style={{ display: "flex" }}>
                  <div>
                    <img
                      src="https://img.icons8.com/metro/26/000000/note.png"
                      alt="task bullet"
                      style={{ width: "10px", margin: "3px 3px 0px 0px" }}
                    />
                  </div>
                  <div>
                    {comment.attachment ? (
                      <a href={comment.attachment.file_url}>
                        Attachment: {comment.attachment.file_name}
                      </a>
                    ) : null}
                    {comment.content}
                  </div>
                </li>
              ))}
            </ul>
          ) : null}
        </div>
        {/* {task.comments.length !== 0 && task.comments[0] !== undefined
          ? task.comments.map((comment) => <div>{comment.content}</div>)
          : null}{" "} */}
      </div>
      {nestedTasks}
    </div>
  );
}
