import React, { useState, useEffect, useCallback } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Header from "./Header";
import moment from "moment";
import axios from "axios";
import Task from "./Task";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    // flexWrap: "wrap",
    flexDirection: "row",
    justifyContent: "center",
    "& > *": {
      margin: theme.spacing(1),
      padding: theme.spacing(3),
      width: "794px",
      height: "1123px",
    },
  },
}));

export default function A4Paper() {
  const classes = useStyles();
  const colours = {
    "30": "#b8255f",
    "36": "#299438",
    "38": "#158fad",
    "47": "#808080",
  };

  const [dateFrom, setdateFrom] = useState(moment().subtract(7, "days"));

  const handleDateChange = (newDate) => {
    console.log(newDate);
    setdateFrom(moment(newDate));
  };

  const [activities, setActivities] = useState([]);

  useEffect(() => {
    axios
      .get(
        "https://api.todoist.com/sync/v8/activity/get?token=token&object_type=item&event_type=completed&parent_project_id=2058245971"
      )
      .then((res) => {
        setActivities(res.data.events);
      });
  }, []);

  const [activeTasks, setActiveTasks] = useState([]);

  useEffect(() => {
    axios
      .get("https://api.todoist.com/rest/v1/tasks?project_id=2058245971", {
        headers: {
          Authorization: "Bearer token",
        },
      })
      .then((res) => setActiveTasks(res.data));
  }, []);

  const getComments = useCallback(async (task_id) => {
    // const getComments = async (task_id) => {
    const comments = await axios.get(
      `https://api.todoist.com/rest/v1/comments?task_id=${task_id}`,
      {
        headers: {
          Authorization: "Bearer token",
        },
      }
    );
    return comments.data;
  }, []);

  const [labels, setLabels] = useState([]);
  useEffect(() => {
    axios
      .get("https://api.todoist.com/rest/v1/labels", {
        headers: {
          Authorization: "Bearer token",
        },
      })
      .then((res) => setLabels(res.data));
  }, []);

  // const buildTree = useCallback((data) => {
  const buildTree = (data) => {
    const store = new Map(); // stores data indexed by it's id
    const rels = new Map(); // stores array of children associated with id
    const roots = []; // stores root nodes
    data.forEach((d) => {
      store.set(d.id, d);
      !rels.get(d.id) ? rels.set(d.id, []) : rels.set(d.id, undefined); // noOp.;
      if (!d.parent_id) {
        roots.push(d.id);
        return;
      }
      const parent = rels.get(d.parent_id) || [];
      parent.push(d.id);
      rels.set(d.parent_id, parent);
    });

    function build(id) {
      const data = store.get(id);
      const children = rels.get(id);
      if (children.length === 0) {
        return { ...data, children: [] };
      }
      return { ...data, children: children.map((c) => build(c)) };
    }
    return roots.map((r) => build(r));
  };
  // }, []);

  const [allTasks, setAllTasks] = useState([]);
  const [nestedAllTasks, setNestedAllTasks] = useState([]);
  // const [allComments, setAllComments] = useState([]);
  // const allCommentsRef = useRef([]);
  // allCommentsRef.current = allComments;

  useEffect(() => {
    // Append active tasks to list

    const getData = async () => {
      let tasks = await Promise.all(
        activeTasks.map(async (task) => {
          return {
            id: task.id,
            parent_id: task.parent_id,
            content: task.content,
            priority: task.priority,
            labels: task.label_ids.map(
              (label_id) => labels.filter((label) => label.id === label_id)[0]
            ),
            comments: await getComments(task.id),
            complete: false,
          };
        })
      );

      setAllTasks(tasks);
      setNestedAllTasks(buildTree(tasks));
    };

    getData();

    // let tasks = [];
    // activeTasks.map(async (task) =>
    //   task.content !== "Complete timesheet"
    //     ? tasks.push({
    //         id: task.id,
    //         parent_id: task.parent_id,
    //         content: task.content,
    //         priority: task.priority,
    //         labels: task.label_ids.map(
    //           (label_id) => labels.filter((label) => label.id === label_id)[0]
    //         ),
    //         comments: await getComments(task.id),
    //         complete: false,
    //       })
    //     : null
    // );

    // const getAllComments = async () => {
    //   tasks.map(async (task) => {
    //     const res = await getComments(task.id);
    //     task.comments = res.data;
    //   });
    // };

    // getAllComments();

    // Append completed tasks to that list also
    // activities.map((activity) =>
    //   activity.extra_data.content !== "Complete timesheet"
    //     ? tasks.push({
    //         id: activity.id,
    //         parent_id: activity.parent_item_id,
    //         content: activity.extra_data.content,
    //         priority: "1",
    //         labels: [],
    //         complete: true,
    //       })
    //     : null
    // );
  }, [activeTasks, activities, labels, getComments]);

  // useEffect(() => {
  //   let tasks = [...allTasks];
  //   // console.log("tasks", tasks);
  //   // const getAllComments = () => {
  //   //   tasks.map(async (task) => {
  //   //     // console.log("task", task);
  //   //     await getComments(task.id)
  //   //       .then((res) => (task.comments = res.data))
  //   //       .then(setNestedAllTasks(buildTree(tasks)));
  //   //   });
  //   // };

  //   const getAllComments = async () => {
  //     tasks.map(async (task) => {
  //       const res = await getComments(task.id);
  //       task.comments = res.data;
  //     });
  //   };

  //   getAllComments();
  //   // setAllTasks(tasks);
  //   //   // console.log("tasks", tasks);
  //   //   // setNestedAllTasks(buildTree(tasks));
  //   //   // const _nestedAllTasks = buildTree(tasks);
  //   //   // setNestedAllTasks(_nestedAllTasks);
  //   //   // setNestedAllTasks(buildTree(allTasks));
  // }, [getComments, allTasks]);

  return (
    <div className={classes.root}>
      <Paper elevation={3}>
        <Header handleDateChange={handleDateChange} dateFrom={dateFrom} />
        {console.log("allTasks", allTasks)}
        {nestedAllTasks.length !== 0 ? (
          nestedAllTasks.map((task) => {
            return <Task key={task.id} task={task} colours={colours} />;
          })
        ) : (
          <p>Loading active tasks...</p>
        )}
      </Paper>
    </div>
  );
}
