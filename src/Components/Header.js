import React, { useState } from "react";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import moment from "moment";
import DateFnsUtils from "@date-io/date-fns";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from "@material-ui/pickers";

const useStyles = makeStyles({
  root: {
    width: "100%",
    // display: "flex",
    // justifyContent: "center",
    textAlign: "center",
    // maxWidth: 500,
  },
});

export default function Header({ dateFrom, handleDateChange }) {
  const classes = useStyles();

  const [editable, setEditable] = useState(false);

  const toggleEditable = () => {
    setEditable(!editable);
  };

  const handleDateSelection = (value) => {
    toggleEditable();
    handleDateChange(value);
  };

  return (
    <div className={classes.root}>
      <Typography variant="h4" gutterBottom>
        Outstanding and completed tasks from
      </Typography>
      {editable ? (
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
          <KeyboardDatePicker
            disableToolbar
            variant="inline"
            format="dd-mm-yyyy"
            margin="none"
            id="date-picker-inline"
            label="Date from"
            value={dateFrom}
            onChange={handleDateSelection}
            KeyboardButtonProps={{
              "aria-label": "change date",
            }}
          />
        </MuiPickersUtilsProvider>
      ) : (
        <Typography variant="h4" component="span" onClick={toggleEditable}>
          {moment(dateFrom).format("DD-MMM-YYYY")}
        </Typography>
      )}
      <Typography variant="h4" component="span">
        {" "}
        to {moment().format("DD-MMM-YYYY")}
      </Typography>
    </div>
  );
}
